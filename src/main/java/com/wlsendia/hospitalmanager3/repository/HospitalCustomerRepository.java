package com.wlsendia.hospitalmanager3.repository;

import com.wlsendia.hospitalmanager3.entity.HospitalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalCustomerRepository extends JpaRepository<HospitalCustomer, Long> {
}

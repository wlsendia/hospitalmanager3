package com.wlsendia.hospitalmanager3.controller;

import com.wlsendia.hospitalmanager3.entity.ClinicHistory;
import com.wlsendia.hospitalmanager3.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class HistoryController {

    private final HistoryService historyService;
    public List<ClinicHistory> getHistoriesByDate(@RequestBody LocalDate request) {

        return historyService.getHistoriesByDate(request);
    }

    public void getHistoryById() {

    }

    public void getNeedBillCorpByDate() {

    }

    public void setHistory() {

    }
}

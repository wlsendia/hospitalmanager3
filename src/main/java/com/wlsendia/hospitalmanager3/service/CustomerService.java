package com.wlsendia.hospitalmanager3.service;

import com.wlsendia.hospitalmanager3.controller.CustomerController;
import com.wlsendia.hospitalmanager3.entity.HospitalCustomer;
import com.wlsendia.hospitalmanager3.model.JoinModel;
import com.wlsendia.hospitalmanager3.repository.ClinicHistoryRepository;
import com.wlsendia.hospitalmanager3.repository.HospitalCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final HospitalCustomerRepository cstRepo;
    private final ClinicHistoryRepository hstRepo;

    public void setCustomer(JoinModel request) { // 리퀘스트바디-회원가입 모델 / 엔티티에 저장
        HospitalCustomer addItem = new HospitalCustomer();

        addItem.setCustomerName(request.getCustomerName());
        addItem.setPhone(request.getPhone());
        addItem.setMemo(request.getMemo());
        addItem.setAddress(request.getAddress());
        addItem.setResidentNum(request.getResidentNum());
        addItem.setJoinDate(LocalDate.now());

        cstRepo.save(addItem);


    }
    public HospitalCustomer getCustomerData(long id) { // id / 손님 정보
        return cstRepo.findById(id).orElseThrow();
    }

}

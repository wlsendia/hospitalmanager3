package com.wlsendia.hospitalmanager3.service;

import com.wlsendia.hospitalmanager3.entity.ClinicHistory;
import com.wlsendia.hospitalmanager3.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final ClinicHistoryRepository hstRepo;

    public List<ClinicHistory> getHistoriesByDate(LocalDate request) { // 날짜 / 모든 진료기록
        return hstRepo.findAllByCareDateOrderByIdDesc(request);

    }

    public void getHistoryById() { // Id / id에 맞는 모든 진료기

    }

    public void getNeedBillCorpByDate() { // 날짜 / isSalary true, isCalculate true, nonSalary-onSalary

    }

    public void setHistory() { // HistoryRequest

    }
}

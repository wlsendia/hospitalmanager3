package com.wlsendia.hospitalmanager3.repository;

import com.wlsendia.hospitalmanager3.entity.ClinicHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ClinicHistoryRepository extends JpaRepository<ClinicHistory, Long> {
    List<ClinicHistory> findAllByCareDateOrderByIdDesc(LocalDate careDate);
}

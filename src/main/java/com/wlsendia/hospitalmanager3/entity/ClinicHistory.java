package com.wlsendia.hospitalmanager3.entity;

import com.wlsendia.hospitalmanager3.enums.CareItem;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class ClinicHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // 고객id 진료항목 비용 급여여부 치료일 치료시간 정산여부
    // getHospitalCustomer().getCustomerName();
    //

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospitalCustomerId", nullable = false)
    private HospitalCustomer hospitalCustomer;
//  customerName;
//  customerPhone;
//  residentRegistrationNumber;
//  address;
//  memo;
//  joinDate;

    private CareItem careItem;
    private Double customerPrice;
    private Boolean isSalary;
    private LocalDate careDate;
    private LocalTime careTime;
    private Boolean isCalculate;


}

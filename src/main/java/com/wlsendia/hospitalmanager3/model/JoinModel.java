package com.wlsendia.hospitalmanager3.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class JoinModel {
    private String customerName;

    //    customerName Phone residentRegistrationNumber address memo joinDate
    private String phone;
    private String residentNum;
    private String address;
    private String memo;//text
}

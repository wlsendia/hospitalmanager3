package com.wlsendia.hospitalmanager3.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class HospitalCustomer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //    customerName customerPhone residentRegistrationNumber address memo joinDate
    private String customerName;
    private String phone;
    private String residentNum;
    private String address;
    private String memo;
    private LocalDate joinDate;
}

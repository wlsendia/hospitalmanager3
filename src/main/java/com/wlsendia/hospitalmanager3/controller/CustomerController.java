package com.wlsendia.hospitalmanager3.controller;

import com.wlsendia.hospitalmanager3.entity.HospitalCustomer;
import com.wlsendia.hospitalmanager3.model.JoinModel;
import com.wlsendia.hospitalmanager3.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

// 프라이스 코스트
// 주민번호 숫자로 바꿀까?
// 엔티티 콜티
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/3")
public class CustomerController {

    private final CustomerService cstService;

    @PostMapping("/set")
    public String setCustomer(@RequestBody @Valid JoinModel request) {
        cstService.setCustomer(request);

        return request.getCustomerName();
    }

    @GetMapping("/cst-date/{id}")
    public HospitalCustomer getCustomerData(@PathVariable Long id) {
        return cstService.getCustomerData(id);
    }
}
